这是一个基于拼音、字体结构、笔划的汉字 embedding 方案。
1. 文件夹 01_pg_create_table 用于在 pg v14 以上数据库中创建表结构；
2. 文件夹 02_pg_import_table 用于向表中导入数据；
   导入表数据的配置属性：
     Format: csv
     Encoding: UTF8
     Delimiter: |