-- cn_chr 该表内数据来自与互联网公开资料爬取，首先用于 NLP 模型训练的 token embadding 制作。
-- 需谨慎当做非正式的汉语字典使用。
-- 汉语言的完整准确资料，请以中国大陆官方发布为准
drop table if exists tb_lang_cn_chr_dic;
create table tb_lang_cn_chr_dic
(
  cn_chr                              char(1)
, cn_spells                           varchar(8)[]
, cn_co_fix                           char(1)
, cn_write_cnt                        int
, cn_write_ord_no                     varchar(64)
, cn_write_struct                     varchar(8)
, is_compatibility_zone_outof_f900    boolean
, cn_write_ex_ord                     varchar(16)[]
, cn_write_ex_ord_no                  text
, primary key (cn_chr)        
)
;
comment on table  tb_lang_cn_chr_dic                                        is '中文字符清单';
comment on column tb_lang_cn_chr_dic.cn_chr                                 is '中文 unicode 字符'              ;
comment on column tb_lang_cn_chr_dic.cn_spells                              is '拼音(包括多音字拼音)'           ;
comment on column tb_lang_cn_chr_dic.cn_co_fix                              is '偏旁'                           ;
comment on column tb_lang_cn_chr_dic.cn_write_cnt                           is '笔画数'                         ;
comment on column tb_lang_cn_chr_dic.cn_write_ord_no                        is '笔顺序号。基本笔画：横竖撇捺折' ;
comment on column tb_lang_cn_chr_dic.cn_write_struct                        is '字体结构'                       ;
comment on column tb_lang_cn_chr_dic.is_compatibility_zone_outof_f900       is '字符是否超出 unicode F900'      ;
comment on column tb_lang_cn_chr_dic.cn_write_ex_ord                        is '笔顺名'                         ;
comment on column tb_lang_cn_chr_dic.cn_write_ex_ord_no                     is '笔顺序号'                       ;
-- cn_write_ord_no 编码含义: 
--   1: 横。包括提，以及稍微有些斜的横，如“七”中的横。
--   2: 竖。包括竖钩。
--   3: 撇。包括长撇和短撇，平撇。
--   4: 捺。包括捺。
--   5: 折。包括带右钩的竖（如“长”的第三笔）和各种带折笔的笔画。
-- cn_write_ex_ord_no 编码含义:
--   a: 弯钩
--   b: 捺
--   c: 提
--   d: 撇
--   e: 撇折
--   f: 撇点
--   g: 斜钩
--   h: 横
--   i: 横折
--   j: 横折弯钩/横斜钩
--   k: 横折折折
--   l: 横折折折钩/横撇弯钩
--   m: 横折折撇
--   n: 横折折/横折弯
--   o: 横折提
--   p: 横折钩
--   q: 横撇/横钩
--   r: 点
--   s: 竖
--   t: 竖弯钩
--   u: 竖折折钩
--   v: 竖折撇/竖折折
--   w: 竖折/竖弯
--   x: 竖提
--   y: 竖钩

-- ------------------------------------------------------------------------------------------------------------------
-- cn_write_bpe
drop table if exists tb_cn_chr_write_bpe;
create table tb_cn_chr_write_bpe
(
  cn_chr                              char(1)
, write_bpe                           text
, write_bpe_ex                        text
, primary key (cn_chr)        
)
;
comment on table  tb_cn_chr_write_bpe                  is '中文字符笔划 bpe';
comment on column tb_cn_chr_write_bpe.cn_chr           is '中文 unicode 字符'              ;
comment on column tb_cn_chr_write_bpe.write_bpe        is '基本笔划 bpe'                   ;
comment on column tb_cn_chr_write_bpe.write_bpe_ex     is '完整笔划 bpe'                   ;

-- -- bpe 的生成:
-- insert into tb_cn_chr_write_bpe(cn_chr)
-- select cn_chr from tb_lang_cn_chr_dic where cn_write_ord_no is not null or cn_write_ex_ord_no is not null order by cn_chr;
-- 
-- with 
-- cte_seq_code as
-- (
--   select 
--     cn_chr
--   , row_number() over(order by cn_chr) as a_no
--   , cn_write_ex_ord_no 
--   from tb_lang_cn_chr_dic 
--   where cn_write_ex_ord_no is not null
--   order by cn_chr 
--   -- limit 100
-- ),
-- cte_bpe_arr as 
-- (
--   select 
--     o_ret_seq_code  
--   , o_ret_token_list
--   from 
--     sm_sc.ft_bpe
--     (
--       (
--         select array_agg(cn_write_ex_ord_no) 
--         from cte_seq_code
--       )
--     , 5000   -- 循环次数
--     , 2048   -- 字典规模                                                           -- 字典里多少类偏旁
--     , 4      -- bpe 后，seq 长度，包含 token 个数                                  -- 一个汉字(seq)包含多少个偏旁结构
--     , 3      -- bpe 后，一个 seq 中，各个 token 包含原始 sub-token 的个数均值      -- 偏旁笔划数
--     )
-- )
-- update tb_cn_chr_write_bpe tb_a_tar
-- set 
--   write_bpe_ex = tb_a_sour.o_ret_seq_code[a_no]
-- from cte_bpe_arr tb_a_sour, cte_seq_code tb_a_idx
-- where tb_a_idx.cn_chr = tb_a_tar.cn_chr
-- ;
-- 
-- -- 如果按照 write_bpe_ex 的 25 种笔划，在每个字的每部分偏旁做统计，将有大量的零值，造成稀疏矩阵的资源浪费
-- -- 所以权衡后，采用 write_bpe_ex 做 bpe，再按上诉规则聚合统计为 5 种基本笔划的数量，记录在 write_bpe 字段，当做 embedding 的笔划部分。
-- update tb_cn_chr_write_bpe
-- set 
--   write_bpe = write_bpe_ex
-- ;
-- update tb_cn_chr_write_bpe
-- set 
--   write_bpe = 
--     replace 
--     (
--       write_bpe
--     , 'a'
--     , '5'
--     )
-- ;
-- update tb_cn_chr_write_bpe
-- set 
--   write_bpe = 
--     replace 
--     (
--       write_bpe
--     , 'b'
--     , '4'
--     )
-- ;
-- update tb_cn_chr_write_bpe
-- set 
--   write_bpe = 
--     replace 
--     (
--       write_bpe
--     , 'c'
--     , '1'
--     )
-- ;
-- update tb_cn_chr_write_bpe
-- set 
--   write_bpe = 
--     replace 
--     (
--       write_bpe
--     , 'd'
--     , '3'
--     )
-- ;
-- update tb_cn_chr_write_bpe
-- set 
--   write_bpe = 
--     replace 
--     (
--       write_bpe
--     , 'e'
--     , '5'
--     )
-- ;
-- update tb_cn_chr_write_bpe
-- set 
--   write_bpe = 
--     replace 
--     (
--       write_bpe
--     , 'f'
--     , '5'
--     )
-- ;
-- update tb_cn_chr_write_bpe
-- set 
--   write_bpe = 
--     replace 
--     (
--       write_bpe
--     , 'g'
--     , '5'
--     )
-- ;
-- update tb_cn_chr_write_bpe
-- set 
--   write_bpe = 
--     replace 
--     (
--       write_bpe
--     , 'h'
--     , '1'
--     )
-- ;
-- update tb_cn_chr_write_bpe
-- set 
--   write_bpe = 
--     replace 
--     (
--       write_bpe
--     , 'i'
--     , '5'
--     )
-- ;
-- update tb_cn_chr_write_bpe
-- set 
--   write_bpe = 
--     replace 
--     (
--       write_bpe
--     , 'j'
--     , '5'
--     )
-- ;
-- update tb_cn_chr_write_bpe
-- set 
--   write_bpe = 
--     replace 
--     (
--       write_bpe
--     , 'k'
--     , '5'
--     )
-- ;
-- update tb_cn_chr_write_bpe
-- set 
--   write_bpe = 
--     replace 
--     (
--       write_bpe
--     , 'l'
--     , '5'
--     )
-- ;
-- update tb_cn_chr_write_bpe
-- set 
--   write_bpe = 
--     replace 
--     (
--       write_bpe
--     , 'm'
--     , '5'
--     )
-- ;
-- update tb_cn_chr_write_bpe
-- set 
--   write_bpe = 
--     replace 
--     (
--       write_bpe
--     , 'n'
--     , '5'
--     )
-- ;
-- update tb_cn_chr_write_bpe
-- set 
--   write_bpe = 
--     replace 
--     (
--       write_bpe
--     , 'o'
--     , '5'
--     )
-- ;
-- update tb_cn_chr_write_bpe
-- set 
--   write_bpe = 
--     replace 
--     (
--       write_bpe
--     , 'p'
--     , '5'
--     )
-- ;
-- update tb_cn_chr_write_bpe
-- set 
--   write_bpe = 
--     replace 
--     (
--       write_bpe
--     , 'q'
--     , '5'
--     )
-- ;
-- update tb_cn_chr_write_bpe
-- set 
--   write_bpe = 
--     replace 
--     (
--       write_bpe
--     , 'r'
--     , '4'
--     )
-- ;
-- update tb_cn_chr_write_bpe
-- set 
--   write_bpe = 
--     replace 
--     (
--       write_bpe
--     , 's'
--     , '2'
--     )
-- ;
-- update tb_cn_chr_write_bpe
-- set 
--   write_bpe = 
--     replace 
--     (
--       write_bpe
--     , 't'
--     , '5'
--     )
-- ;
-- update tb_cn_chr_write_bpe
-- set 
--   write_bpe = 
--     replace 
--     (
--       write_bpe
--     , 'u'
--     , '5'
--     )
-- ;
-- update tb_cn_chr_write_bpe
-- set 
--   write_bpe = 
--     replace 
--     (
--       write_bpe
--     , 'v'
--     , '5'
--     )
-- ;
-- update tb_cn_chr_write_bpe
-- set 
--   write_bpe = 
--     replace 
--     (
--       write_bpe
--     , 'w'
--     , '5'
--     )
-- ;
-- update tb_cn_chr_write_bpe
-- set 
--   write_bpe = 
--     replace 
--     (
--       write_bpe
--     , 'x'
--     , '5'
--     )
-- ;
-- update tb_cn_chr_write_bpe
-- set 
--   write_bpe = 
--     replace 
--     (
--       write_bpe
--     , 'y'
--     , '2'
--     )
-- ;

-- ------------------------------------------------------------------------------------------------------------------
-- drop table if exists tb_tmp_cn_token;
create table tb_tmp_cn_token
(
  cn_no                 serial             primary key       
, cn_token              varchar(32) 
, cn_token_embedding    float[]
);
comment on table  tb_tmp_cn_token                      is '中文字符粒度的 token embedding' ;
comment on column tb_tmp_cn_token.cn_no                is '自增序号'                       ;
comment on column tb_tmp_cn_token.cn_token             is '中文 unicode 字符'              ;
comment on column tb_tmp_cn_token.cn_token_embedding   is 'embedding'                      ;

-- -- embedding 的构建
-- insert into tb_tmp_cn_token
-- (
--   cn_no
-- , cn_token
-- , cn_token_embedding
-- )
-- with 
-- cte_spell as 
-- (
--   select 
--     tb_a_main.cn_chr
--   , sm_sc.fv_regexp_matches_strs_once(lower(a_spell), '^([bpmfdtnlgkhjqxryw]|[zcs]h?|er)?(i(?=[^0-5en])|u(?=[^0-5ien]))?(a(?:i|o)?|ou?|ei?|i(?:u|e)?|u(?:e|i)?|ve?|[aeiuv]n|[aeio]?ng)?([0-5])$', 'i') as a_spell
--   from tb_lang_cn_chr_dic tb_a_main, unnest(cn_spells) tb_a(a_spell)
--   where tb_a_main.cn_spells is not null
-- ),
-- cte_spell_encoder_aggr as 
-- (
--   select 
--     cn_chr
--   , array_agg(distinct a_spell[1])                  as  a_spell_fix_1
--   , array_agg(distinct a_spell[2])                  as  a_spell_fix_2
--   , array_agg(distinct left(a_spell[3], 1))         as  a_spell_fix_3
--   , array_agg(distinct substr(a_spell[3], 2, 1))    as  a_spell_fix_4
--   , array_agg(distinct substr(a_spell[3], 2))       as  a_spell_fix_5
--   , array_agg(distinct a_spell[4])                  as  a_spell_fix_6
--   , count(*)                                        as  a_spell_cnt
--   from cte_spell tb_a_chr
--   group by cn_chr
-- ),
-- cte_spell_encoder as 
-- (
--   select 
--     tb_a_main.cn_chr
--   , case when 'b'  = any(a_spell_fix_1) then 1.0 else 0.0 end        as a_cn_chr_encode_0001
--   , case when 'w'  = any(a_spell_fix_1) then 1.0 else 0.0 end        as a_cn_chr_encode_0002
--   , case when 'd'  = any(a_spell_fix_1) then 1.0 else 0.0 end        as a_cn_chr_encode_0003
--   , case when 'g'  = any(a_spell_fix_1) then 1.0 else 0.0 end        as a_cn_chr_encode_0004
--   , case when 'zh' = any(a_spell_fix_1) then 1.0 else 0.0 end        as a_cn_chr_encode_0005
--   , case when 'ch' = any(a_spell_fix_1) then 1.0 else 0.0 end        as a_cn_chr_encode_0006
--   , case when 'sh' = any(a_spell_fix_1) then 1.0 else 0.0 end        as a_cn_chr_encode_0007
--   , case when 'r'  = any(a_spell_fix_1) then 1.0 else 0.0 end        as a_cn_chr_encode_0008
--   , case when 'j'  = any(a_spell_fix_1) then 1.0 else 0.0 end        as a_cn_chr_encode_0009
--   , case when 'h'  = any(a_spell_fix_1) then 1.0 else 0.0 end        as a_cn_chr_encode_0010
--   , case when 'm'  = any(a_spell_fix_1) then 1.0 else 0.0 end        as a_cn_chr_encode_0011
--   , case when 'q'  = any(a_spell_fix_1) then 1.0 else 0.0 end        as a_cn_chr_encode_0012
-- 
--   , case when 'p'  = any(a_spell_fix_1) then 1.0 else 0.0 end        as a_cn_chr_encode_0013
--   , case when 'f'  = any(a_spell_fix_1) then 1.0 else 0.0 end        as a_cn_chr_encode_0014
--   , case when 't'  = any(a_spell_fix_1) then 1.0 else 0.0 end        as a_cn_chr_encode_0015
--   , case when 'k'  = any(a_spell_fix_1) then 1.0 else 0.0 end        as a_cn_chr_encode_0016
--   , case when 'z'  = any(a_spell_fix_1) then 1.0 else 0.0 end        as a_cn_chr_encode_0017
--   , case when 'c'  = any(a_spell_fix_1) then 1.0 else 0.0 end        as a_cn_chr_encode_0018
--   , case when 's'  = any(a_spell_fix_1) then 1.0 else 0.0 end        as a_cn_chr_encode_0019
--   , case when 'er' = any(a_spell_fix_1) then 1.0 else 0.0 end        as a_cn_chr_encode_0020
--   , case when 'y'  = any(a_spell_fix_1) then 1.0 else 0.0 end        as a_cn_chr_encode_0021
--   , case when 'l'  = any(a_spell_fix_1) then 1.0 else 0.0 end        as a_cn_chr_encode_0022
--   , case when 'n'  = any(a_spell_fix_1) then 1.0 else 0.0 end        as a_cn_chr_encode_0023
--   , case when 'x'  = any(a_spell_fix_1) then 1.0 else 0.0 end        as a_cn_chr_encode_0024
-- 
--   , case when 'i'  = any(a_spell_fix_2) then 1.0 else 0.0 end        as a_cn_chr_encode_0025
--   , case when 'u'  = any(a_spell_fix_2) then 1.0 else 0.0 end        as a_cn_chr_encode_0026
-- 
--   , case when 'a'  = any(a_spell_fix_3) then 1.0 else 0.0 end        as a_cn_chr_encode_0027
--   , case when 'v'  = any(a_spell_fix_3) then 1.0 else 0.0 end        as a_cn_chr_encode_0028
--   , case when 'o'  = any(a_spell_fix_3) then 1.0 else 0.0 end        as a_cn_chr_encode_0029
--   , case when 'e'  = any(a_spell_fix_3) then 1.0 else 0.0 end        as a_cn_chr_encode_0030
--   , case when 'i'  = any(a_spell_fix_3) then 1.0 else 0.0 end        as a_cn_chr_encode_0031
--   , case when 'u'  = any(a_spell_fix_3) then 1.0 else 0.0 end        as a_cn_chr_encode_0032
-- 
--   , case when 'o'  = any(a_spell_fix_4) then 1.0 else 0.0 end        as a_cn_chr_encode_0033
--   , case when 'e'  = any(a_spell_fix_4) then 1.0 else 0.0 end        as a_cn_chr_encode_0034
--   , case when 'i'  = any(a_spell_fix_4) then 1.0 else 0.0 end        as a_cn_chr_encode_0035
--   , case when 'u'  = any(a_spell_fix_4) then 1.0 else 0.0 end        as a_cn_chr_encode_0036
-- 
--   , case when 'n'  = any(a_spell_fix_5) then 1.0 else 0.0 end        as a_cn_chr_encode_0037
--   , case when 'ng' = any(a_spell_fix_5) then 1.0 else 0.0 end        as a_cn_chr_encode_0038
-- 
--   , case when '1'  = any(a_spell_fix_6) then 1.0 else 0.0 end        as a_cn_chr_encode_0039
--   , case when '2'  = any(a_spell_fix_6) then 1.0 else 0.0 end        as a_cn_chr_encode_0040
--   , case when '3'  = any(a_spell_fix_6) then 1.0 else 0.0 end        as a_cn_chr_encode_0041
--   , case when '4'  = any(a_spell_fix_6) then 1.0 else 0.0 end        as a_cn_chr_encode_0042
--   , case when '5'  = any(a_spell_fix_6) then 1.0 else 0.0 end        as a_cn_chr_encode_0043
--   , a_spell_cnt :: float                                             as a_cn_chr_encode_0044
--   , case cn_write_struct
--       when '上下结构'
--         then 1.0
--       when '上中下结构'
--         then 2.0
--       else 0.0
--     end                                                                                                 as a_cn_chr_encode_0045    -- 上下结构
--   , case cn_write_struct
--       when '左右结构'
--         then 1.0
--       when '左中右结构'
--         then 2.0
--       else 0.0
--     end                                                                                                 as a_cn_chr_encode_0046    -- 左右结构
--   , case 
--       when cn_write_struct = '上包围结构'
--         then 2.0
--       when cn_write_struct in ('左上包围结构', '右上包围结构', '全包围结构')
--         then 1.0
--       else 0.0                                                                                          
--     end                                                                                                 as a_cn_chr_encode_0047    -- 上包围
--   , case 
--       when cn_write_struct = '下包围结构'
--         then 2.0
--       when cn_write_struct in ('左下包围结构', '全包围结构')
--         then 1.0
--       else 0.0                                                                                          
--     end                                                                                                 as a_cn_chr_encode_0048    -- 下包围
--   , case 
--       when cn_write_struct = '左包围结构'
--         then 2.0
--       when cn_write_struct in ('左上包围结构', '左下包围结构', '上包围结构', '下包围结构', '全包围结构')
--         then 1.0
--       else 0.0                                                                                          
--     end                                                                                                 as a_cn_chr_encode_0049    -- 左包围
--   , case 
--       when cn_write_struct in ('右上包围结构', '右下包围结构', '上包围结构', '下包围结构', '全包围结构')
--         then 1.0
--       else 0.0                                                                                          
--     end                                                                                                 as a_cn_chr_encode_0050    -- 右包围
--   , case cn_write_struct
--       when '独体字'
--         then 3.0
--       when '镶嵌结构'
--         then 2.0
--       when '品字结构'
--         then 1.0
--       else 0.0                                                                                          
--     end                                                                                                 as a_cn_chr_encode_0051    -- 紧凑性
--   -- 各部分各类笔划的数量当作编码位，横竖撇捺折，依次共五位编码；三个 token 共 15 位
--   , coalesce(length(tb_a_write_bpe.a_cn_bpe_4_encode_token_01) 
--              - length(replace(tb_a_write_bpe.a_cn_bpe_4_encode_token_01, '1', '')) :: float, 0.0)       as a_cn_chr_encode_0052
--   , coalesce(length(tb_a_write_bpe.a_cn_bpe_4_encode_token_01)                                                          
--              - length(replace(tb_a_write_bpe.a_cn_bpe_4_encode_token_01, '2', '')) :: float, 0.0)       as a_cn_chr_encode_0053
--   , coalesce(length(tb_a_write_bpe.a_cn_bpe_4_encode_token_01)                                                          
--              - length(replace(tb_a_write_bpe.a_cn_bpe_4_encode_token_01, '3', '')) :: float, 0.0)       as a_cn_chr_encode_0054
--   , coalesce(length(tb_a_write_bpe.a_cn_bpe_4_encode_token_01)                                                         
--              - length(replace(tb_a_write_bpe.a_cn_bpe_4_encode_token_01, '4', '')) :: float, 0.0)       as a_cn_chr_encode_0055
--   , coalesce(length(tb_a_write_bpe.a_cn_bpe_4_encode_token_01)                                                         
--              - length(replace(tb_a_write_bpe.a_cn_bpe_4_encode_token_01, '5', '')) :: float, 0.0)       as a_cn_chr_encode_0056
--   , coalesce(length(tb_a_write_bpe.a_cn_bpe_4_encode_token_02)                                                          
--              - length(replace(tb_a_write_bpe.a_cn_bpe_4_encode_token_02, '1', '')) :: float, 0.0)       as a_cn_chr_encode_0057
--   , coalesce(length(tb_a_write_bpe.a_cn_bpe_4_encode_token_02)                                                          
--              - length(replace(tb_a_write_bpe.a_cn_bpe_4_encode_token_02, '2', '')) :: float, 0.0)       as a_cn_chr_encode_0058
--   , coalesce(length(tb_a_write_bpe.a_cn_bpe_4_encode_token_02)                                                          
--              - length(replace(tb_a_write_bpe.a_cn_bpe_4_encode_token_02, '3', '')) :: float, 0.0)       as a_cn_chr_encode_0059
--   , coalesce(length(tb_a_write_bpe.a_cn_bpe_4_encode_token_02)                                                          
--              - length(replace(tb_a_write_bpe.a_cn_bpe_4_encode_token_02, '4', '')) :: float, 0.0)       as a_cn_chr_encode_0060
--   , coalesce(length(tb_a_write_bpe.a_cn_bpe_4_encode_token_02)                                                          
--              - length(replace(tb_a_write_bpe.a_cn_bpe_4_encode_token_02, '5', '')) :: float, 0.0)       as a_cn_chr_encode_0061
--   , coalesce(length(tb_a_write_bpe.a_cn_bpe_4_encode_token_03)                                                          
--              - length(replace(tb_a_write_bpe.a_cn_bpe_4_encode_token_03, '1', '')) :: float, 0.0)       as a_cn_chr_encode_0062
--   , coalesce(length(tb_a_write_bpe.a_cn_bpe_4_encode_token_03)                                                          
--              - length(replace(tb_a_write_bpe.a_cn_bpe_4_encode_token_03, '2', '')) :: float, 0.0)       as a_cn_chr_encode_0063
--   , coalesce(length(tb_a_write_bpe.a_cn_bpe_4_encode_token_03)                                                          
--              - length(replace(tb_a_write_bpe.a_cn_bpe_4_encode_token_03, '3', '')) :: float, 0.0)       as a_cn_chr_encode_0064
--   , coalesce(length(tb_a_write_bpe.a_cn_bpe_4_encode_token_03) 
--              - length(replace(tb_a_write_bpe.a_cn_bpe_4_encode_token_03, '4', '')) :: float, 0.0)       as a_cn_chr_encode_0065
--   , coalesce(length(tb_a_write_bpe.a_cn_bpe_4_encode_token_03) 
--              - length(replace(tb_a_write_bpe.a_cn_bpe_4_encode_token_03, '5', '')) :: float, 0.0)       as a_cn_chr_encode_0066
--   , case tb_a_main.cn_chr
--       when '《' then '-0.1'
--       when '》' then '0.1'
--       when '“'  then '-0.25'
--       when '”'  then '0.25'
--       when '、' then '0.5'
--       when '：' then '-0.75'
--       when '，' then '1.0'
--       when '；' then '1.25'
--       when '？' then '-1.0'
--       when '。' then '1.5'
--       when '！' then '2.0'
--       else 0.0  
--     end                                                                                                 as a_cn_chr_encode_0067   -- 标点符号
--   , 0.0                                                                                                 as a_cn_chr_encode_0068   -- 语料开始、结束标记
--   , case when tb_a_main.cn_chr in ('《', '》', '“', '”', '、', '：', '，', '；', '？', '。', '！') then 0.0 else random() end   as a_cn_chr_encode_0069   -- 随机位
--   , case when tb_a_main.cn_chr in ('《', '》', '“', '”', '、', '：', '，', '；', '？', '。', '！') then 0.0 else random() end   as a_cn_chr_encode_0070   -- 随机位
--   , case when tb_a_main.cn_chr in ('《', '》', '“', '”', '、', '：', '，', '；', '？', '。', '！') then 0.0 else random() end   as a_cn_chr_encode_0071   -- 随机位
--   , case when tb_a_main.cn_chr in ('《', '》', '“', '”', '、', '：', '，', '；', '？', '。', '！') then 0.0 else random() end   as a_cn_chr_encode_0072   -- 随机位
--   , case when tb_a_main.cn_chr in ('《', '》', '“', '”', '、', '：', '，', '；', '？', '。', '！') then 0.0 else random() end   as a_cn_chr_encode_0073   -- 随机位
--   , case when tb_a_main.cn_chr in ('《', '》', '“', '”', '、', '：', '，', '；', '？', '。', '！') then 0.0 else random() end   as a_cn_chr_encode_0074   -- 随机位
--   , case when tb_a_main.cn_chr in ('《', '》', '“', '”', '、', '：', '，', '；', '？', '。', '！') then 0.0 else random() end   as a_cn_chr_encode_0075   -- 随机位
--   , case when tb_a_main.cn_chr in ('《', '》', '“', '”', '、', '：', '，', '；', '？', '。', '！') then 0.0 else random() end   as a_cn_chr_encode_0076   -- 随机位
--   , case when tb_a_main.cn_chr in ('《', '》', '“', '”', '、', '：', '，', '；', '？', '。', '！') then 0.0 else random() end   as a_cn_chr_encode_0077   -- 随机位
--   , case when tb_a_main.cn_chr in ('《', '》', '“', '”', '、', '：', '，', '；', '？', '。', '！') then 0.0 else random() end   as a_cn_chr_encode_0078   -- 随机位
--   , case when tb_a_main.cn_chr in ('《', '》', '“', '”', '、', '：', '，', '；', '？', '。', '！') then 0.0 else random() end   as a_cn_chr_encode_0079   -- 随机位
--   , case when tb_a_main.cn_chr in ('《', '》', '“', '”', '、', '：', '，', '；', '？', '。', '！') then 0.0 else random() end   as a_cn_chr_encode_0080   -- 随机位
--   from tb_lang_cn_chr_dic tb_a_main
--   left join cte_spell_encoder_aggr tb_a_chr
--     on tb_a_chr.cn_chr = tb_a_main.cn_chr
--   left join 
--   (
--     select 
--       tb_a_bpe.cn_chr
--     , case cardinality(regexp_split_to_array(tb_a_bpe.write_bpe, ' '))
--         when 1
--           then null
--         when 2
--           then 
--             case -- 笔划少的部分大概率当作偏旁部首，另一部分当字体结构的主体。
--               when length(split_part(tb_a_bpe.write_bpe, ' ', 1)) > length(split_part(tb_a_bpe.write_bpe, ' ', 2))
--                 then null
--               else split_part(tb_a_bpe.write_bpe, ' ', 1)
--             end
--         when 3
--           then split_part(tb_a_bpe.write_bpe, ' ', 1)          
--       end                                                                                               as a_cn_bpe_4_encode_token_01
--     , case cardinality(regexp_split_to_array(tb_a_bpe.write_bpe, ' '))
--         when 1
--           then tb_a_bpe.write_bpe
--         when 2
--           then 
--             case -- 笔划少的部分大概率当作偏旁部首，另一部分当字体结构的主体。主体部分放在 encode_0041
--               when length(split_part(tb_a_bpe.write_bpe, ' ', 1)) > length(split_part(tb_a_bpe.write_bpe, ' ', 2))
--                 then split_part(tb_a_bpe.write_bpe, ' ', 1)
--               else split_part(tb_a_bpe.write_bpe, ' ', 2)
--             end
--         when 3
--           then split_part(tb_a_bpe.write_bpe, ' ', 2)          
--       end                                                                                               as a_cn_bpe_4_encode_token_02
--     , case cardinality(regexp_split_to_array(tb_a_bpe.write_bpe, ' '))
--         when 1
--           then null
--         when 2
--           then 
--             case -- 笔划少的部分大概率当作偏旁部首，另一部分当字体结构的主体。
--               when length(split_part(tb_a_bpe.write_bpe, ' ', 1)) > length(split_part(tb_a_bpe.write_bpe, ' ', 2))
--                 then split_part(tb_a_bpe.write_bpe, ' ', 2)
--               else null
--             end
--         when 3
--           then split_part(tb_a_bpe.write_bpe, ' ', 3)          
--       end                                                                                               as a_cn_bpe_4_encode_token_03
--     from tb_cn_chr_write_bpe tb_a_bpe
--   ) tb_a_write_bpe
--   on tb_a_write_bpe.cn_chr = tb_a_main.cn_chr
-- )
-- select 
--   row_number() over()   as cn_no 
-- , cn_chr
-- , sm_sc.fv_coalesce
--   (
--     array 
--     [
--       a_cn_chr_encode_0001
--     , a_cn_chr_encode_0002
--     , a_cn_chr_encode_0003
--     , a_cn_chr_encode_0004
--     , a_cn_chr_encode_0005
--     , a_cn_chr_encode_0006
--     , a_cn_chr_encode_0007
--     , a_cn_chr_encode_0008
--     , a_cn_chr_encode_0009
--     , a_cn_chr_encode_0010
--     , a_cn_chr_encode_0011
--     , a_cn_chr_encode_0012
--     , a_cn_chr_encode_0013
--     , a_cn_chr_encode_0014
--     , a_cn_chr_encode_0015
--     , a_cn_chr_encode_0016
--     , a_cn_chr_encode_0017
--     , a_cn_chr_encode_0018
--     , a_cn_chr_encode_0019
--     , a_cn_chr_encode_0020
--     , a_cn_chr_encode_0021
--     , a_cn_chr_encode_0022
--     , a_cn_chr_encode_0023
--     , a_cn_chr_encode_0024
--     , a_cn_chr_encode_0025
--     , a_cn_chr_encode_0026
--     , a_cn_chr_encode_0027
--     , a_cn_chr_encode_0028
--     , a_cn_chr_encode_0029
--     , a_cn_chr_encode_0030
--     , a_cn_chr_encode_0031
--     , a_cn_chr_encode_0032
--     , a_cn_chr_encode_0033
--     , a_cn_chr_encode_0034
--     , a_cn_chr_encode_0035
--     , a_cn_chr_encode_0036
--     , a_cn_chr_encode_0037
--     , a_cn_chr_encode_0038
--     , a_cn_chr_encode_0039
--     , a_cn_chr_encode_0040
--     , a_cn_chr_encode_0041
--     , a_cn_chr_encode_0042
--     , a_cn_chr_encode_0043
--     , a_cn_chr_encode_0044
--     , a_cn_chr_encode_0045
--     , a_cn_chr_encode_0046
--     , a_cn_chr_encode_0047
--     , a_cn_chr_encode_0048
--     , a_cn_chr_encode_0049
--     , a_cn_chr_encode_0050
--     , a_cn_chr_encode_0051
--     , a_cn_chr_encode_0052
--     , a_cn_chr_encode_0053
--     , a_cn_chr_encode_0054
--     , a_cn_chr_encode_0055
--     , a_cn_chr_encode_0056
--     , a_cn_chr_encode_0057
--     , a_cn_chr_encode_0058
--     , a_cn_chr_encode_0059
--     , a_cn_chr_encode_0060
--     , a_cn_chr_encode_0061
--     , a_cn_chr_encode_0062
--     , a_cn_chr_encode_0063
--     , a_cn_chr_encode_0064
--     , a_cn_chr_encode_0065
--     , a_cn_chr_encode_0066
--     , a_cn_chr_encode_0067
--     , a_cn_chr_encode_0068
--     , a_cn_chr_encode_0069
--     , a_cn_chr_encode_0070
--     , a_cn_chr_encode_0071
--     , a_cn_chr_encode_0072
--     , a_cn_chr_encode_0073
--     , a_cn_chr_encode_0074
--     , a_cn_chr_encode_0075
--     , a_cn_chr_encode_0076
--     , a_cn_chr_encode_0077
--     , a_cn_chr_encode_0078
--     , a_cn_chr_encode_0079
--     , a_cn_chr_encode_0080
--     ]
--   , 0.0 :: float
--   )
-- from cte_spell_encoder
-- ;
-- 
-- -- -- -- softmax 归一化
-- -- -- update tb_tmp_cn_token
-- -- -- set cn_token_embedding = sm_sc.fv_redistr_softmax(cn_token_embedding);
-- 
-- -- 内部语料标记字符
-- insert into tb_tmp_cn_token
-- (
--   cn_no
-- , cn_token
-- , cn_token_embedding
-- )
-- select 
--   (select count(*) from tb_tmp_cn_token) + 1
-- , '^'
-- , sm_sc.fv_mx_ele_2d_2_1d(sm_sc.fv_new(0.0 :: float, array[1, 67]))
--   || array[-1.0] :: float[]                             -- a_cn_chr_encode_0068
--   || sm_sc.fv_mx_ele_2d_2_1d(sm_sc.fv_new(0.0 :: float, array[1, 12]))
-- union all 
-- select 
--   (select count(*) from tb_tmp_cn_token) + 2
-- , '$'
-- , sm_sc.fv_mx_ele_2d_2_1d(sm_sc.fv_new(0.0 :: float, array[1, 67]))
--   || array[1.0] :: float[]                              -- a_cn_chr_encode_0068
--   || sm_sc.fv_mx_ele_2d_2_1d(sm_sc.fv_new(0.0 :: float, array[1, 12]))
-- ;
-- 
-- -- 空字符
-- insert into tb_tmp_cn_token
-- (
--   cn_no
-- , cn_token
-- , cn_token_embedding
-- )
-- -- padding mask(空白、长度补齐填充、占位字符)
-- select 
--   (select count(*) from tb_tmp_cn_token) + 1
-- , ' '
-- , sm_sc.fv_mx_ele_2d_2_1d(sm_sc.fv_new(0.0 :: float, array[1, 80]))
-- ;
